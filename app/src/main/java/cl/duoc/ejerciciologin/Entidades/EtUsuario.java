package cl.duoc.ejerciciologin.Entidades;

/**
 * Created by DUOC on 25-03-2017.
 */

public class EtUsuario {
    private String usuario;
    private String nombre;
    private String apellido;
    private String rut;
    private String fechaNacimiento;
    private String clave;
    private String repetirClave;


    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getRepetirClave() {
        return repetirClave;
    }

    public void setRepetirClave(String repetirClave) {
        this.repetirClave = repetirClave;
    }

    public EtUsuario(String usuario, String nombre, String apellido, String rut, String fechaNacimiento, String clave, String repetirClave) {
        this.usuario = usuario;
        this.nombre = nombre;
        this.apellido = apellido;
        this.rut = rut;
        this.fechaNacimiento = fechaNacimiento;
        this.clave = clave;
        this.repetirClave = repetirClave;
    }
}
