package cl.duoc.ejerciciologin;

import android.content.Intent;
import android.support.v4.app.NotificationCompatSideChannelService;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {
        private Button btnEntrar,btnRegistro;
    private EditText etUsuario,etContraseña;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etUsuario=(EditText) findViewById(R.id.etUsuario);
        etContraseña=(EditText) findViewById(R.id.etContraseña);
        btnEntrar=(Button) findViewById(R.id.btnEntrar);
        btnRegistro=(Button) findViewById(R.id.btnRegistro);





        btnEntrar.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v.getId()==R.id.btnEntrar) {
                    if (etContraseña.getText().toString().equals("admin") &&
                            etUsuario.getText().toString().equals("admin")) {
                        Toast.makeText(LoginActivity.this, "Usuario Valido", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(LoginActivity.this, "Usuario no Valido", Toast.LENGTH_LONG).show();
                    }

                }else if (v.getId() == R.id.btnRegistro) {
                    Intent i = new Intent(LoginActivity.this, RegistroActivity.class);
                    startActivity(i);
                }
            }

        }));
        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, RegistroActivity.class);
                startActivity(i);
            }
        });

    }
}
