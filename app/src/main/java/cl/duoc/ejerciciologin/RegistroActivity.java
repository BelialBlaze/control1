package cl.duoc.ejerciciologin;

import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class RegistroActivity extends AppCompatActivity {
    private Button btnAceptar;
    private EditText etUsuario, etNombre,etApellido,etRut,etFechaNacimiento,etClave,etRepetirClave;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        etUsuario=(EditText) findViewById(R.id.etUsuario);
        etNombre=(EditText)findViewById(R.id.etNombre);
        etApellido=(EditText)findViewById(R.id.etApellido);
        etRut=(EditText)findViewById(R.id.etRut);
        etFechaNacimiento=(EditText)findViewById(R.id.etFechaNacimiento);
        etClave=(EditText) findViewById(R.id.etClave);
        etRepetirClave=(EditText)findViewById(R.id.etRepetirClave);
        btnAceptar=(Button) findViewById(R.id.btnAceptar);


        btnAceptar.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getId()==R.id.btnEntrar){

                }
            }
        }));

    }
}
